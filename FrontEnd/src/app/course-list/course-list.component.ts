import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {
  id;
  nama;
  detail;
  sks;
  durasi;
  constructor(private api:APIService) {} 

  ngOnInit() {}
  
  CourseBaru(){
    this.api.CourseBaru(this.id,this.nama,this.durasi,this.sks,this.detail);
  }

}
