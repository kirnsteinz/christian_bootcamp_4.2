import { Injectable } from '@angular/core';

@Injectable()
export class APIService {
  private courseList : Object[] = [
    {"id":"1","Nama":"Matematika","Durasi":"120","SKS": 4, "Detail":"Matematika Lanjut"},
    {"id":"2","Nama":"Fisika","Durasi":"90","SKS": 3, "Detail":"Fisika Relasional"},
    {"id":"3","Nama":"Bahasa Indonesia","Durasi":"60","SKS": 2, "Detail":"Bahasa Indonesia"},
    {"id":"4","Nama":"Bahasa Inggris","Durasi":"60","SKS": 2, "Detail":"Bahasa Inggris"},
    {"id":"5","Nama":"Sejarah","Durasi":"90","SKS": 3, "Detail":"Sejarah Pemrograman"},
    {"id":"6","Nama":"Programming Fundamental","Durasi":"120","SKS": 4, "Detail":"Dasar dan logika Pemrograman"},
    {"id":"7","Nama":"Kewarganegaraan","Durasi":"60","SKS": 2, "Detail":"PPKN"}
  ]
  constructor() { }
  CourseBaru(id,Nama,Durasi,SKS,Detail){
    this.courseList.push({"id":id,"Nama":Nama,"Durasi":Durasi,"SKS": SKS, "Detail":Detail});
  }
}
