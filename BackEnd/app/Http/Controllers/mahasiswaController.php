<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mahasiswa;

class mahasiswaController extends Controller
{
    function mahasiswaBaru(Request $request){
      DB::beginTransaction();
        try{
            $this->validate($request, [
                'nama' => 'required'
            ]);            
            $new = new Mahasiswa;
            $new->nama = $request->input('nama');
            $new->detail = $request->input('detail');
            $new->GPA = 0;
            $new->totalSKS = 0;
            $new->save();

            DB::commit();
            return response()->json($new, 201);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json([$e->getMessage()], 500);
        }
    }
    function editMahasiswa(Request $request){
      DB::beginTransaction();
        try{
            $this->validate($request, [
                'id' => 'required'
            ]);            
            $new = Mahasiswa::where('id',$request->input('id'))->get()->first();
            if ($request->input('nama')){
                $new->nama = $request->input('nama');
            }
            if ($request->input('detail')){
                $new->detail = $request->input('detail');
            }
            $new->save();

            DB::commit();
            return response()->json($new, 201);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json([$e->getMessage()], 500);
        }
    }
}
